package ds;

/**
 * La implementacion de esta interfaz permite que un objeto pueda
 *  hacer uso de estos métodos
 * @author cisco
 */
public interface iQueue<T> {
     /**
     * Devuelve el número de elementos de la pila
     * @return el tamaño de la lista
     */
    public abstract int size();
    /**
     * Indica si la pila está vacío o no
     * @return true si e tamaño es 0
     */
    public abstract boolean isEmpty();
    /**
     *  Añade un nuevo elemento a la cola (al final de la colección)
     * @param obj  objeto que recibe
     */
    public abstract void enqueue(T obj);
    /**
     *  Elimina el siguiente elemento de la cola (al principio de la coleccion
     * y lo devuelve
     * @return el siguiente elemento de la cola
     */
    public abstract Object dequeue();
    /**
     * Devuelve el siguiente elemento de la cola sin eliminarlo
     * @return el siguiente elemento de la cola
     */
    public abstract Object peek();
}
