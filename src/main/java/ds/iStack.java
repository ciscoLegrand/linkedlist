package ds;

/**
 *  La implementacion de esta interfaz permite que un objeto pueda
 *  hacer uso de estos métodos
 * @author cisco
 */
public interface iStack <T>{
     /**
     * Devuelve el número de elementos de la pila
     * @return el tamaño de la lista
     */
    public abstract int size();
    /**
     * Indica si la pila está vacío o no
     * @return true si e tamaño es 0
     */
    public abstract boolean isEmpty();
    /**
     * Añade un nuevo elemento a la pila (al final de la colección)
     * @param obj objeto que recibe
     */
     public abstract void push( T obj);
    /**
     *  Elimina el siguiente elemento de la pila y lo devuelve
     * @return el siguente elemento de la pila 
     */
     public abstract Object pop();
    /**
     * Devuelve el siguiente elemento de la pila sin eliminarlo
     * @return el siguiente elemento de la pila
     */
    public abstract Object peek();
}
