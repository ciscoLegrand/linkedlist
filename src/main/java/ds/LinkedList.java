package ds;

import java.util.Iterator;

/**
 * <tt>LinkedList</tt> o lista enlzada que
 * permite todo tipo de elemntos, incluídos <tt>null</tt>
 * El Iterator esta proporcionado por esta clase {@link #iterator() iterator}
 * @author cisco gonzalez
 * @version 2.2
 * @param <T> el tipo de elementos devueltos por la <tt>LinkedList</tt>
 */
public class LinkedList<T> implements Iterable<T>, iStack<T>, iQueue<T>{
    /**
     * Nodo previo a enlazar
     */
    private ListNode<T> head;
    /**
     * Nodo posterior a enlazar
     */
    private ListNode<T> tail;
    /**
     * numero de nodos en el <tt>LinkedList</tt>
     */
    private int len;
    
    /**
     * Constructor por defecto  <tt>LinkedList</tt>
     */
    public LinkedList(){}
    
    /**
     * Añade un un nuevo nodo a la cabeza de la coleccion
     * @param pNodo nodo a añadir en  <tt>LinkedList</tt>
     */
    private void addHead(T pNodo){
        ListNode<T> nodo = new ListNode(pNodo);
        nodo.next = this.head;
        this.head  = nodo;
        if(this.tail==null) this.tail = this.head;
        len++;
    }
    
    /**
     * Añade un nuvo nodo a la cola de la coleccion
     * @param pNodo nodo a añadir en  <tt>LinkedList</tt>
     */
    private void addTail(T pNodo){
        ListNode<T> nodo = new ListNode(pNodo);
        this.tail.next=nodo;
        this.tail = this.tail.next;
        len++;
    }

   /**
     * Añade a  <tt>LinkedList</tt> un elemento que indicamos como parametro
     * Si la coleccion está vacía se añade a la cabeza
     * si la coleccion contiene nodos se añade a la cola
     * @param pNodo nodo a añadir en  <tt>LinkedList</tt>
     */
    public void add(T pNodo){
        if(isEmpty()) addHead(pNodo);
        else addTail(pNodo);        
    }
    
    /**
     * Añade un elemento al <tt>LinkedList</tt> en la posicion indicada
     * @param pIndex indice de la posicion en la que añadir el nodo
     * @param pNodo nodo a añadir 
     */
    public void add(int pIndex, T pNodo){
        insert(pNodo,pIndex);
    }
    
    /**
     * Elimina el elemento que se pasa por parametro 
     * devuelve true en caso de que el elemento se encuetre
     * y false si no se encuentra
     * @param pNodo
     * @return
     */
    public boolean delete(Object pNodo){
        boolean delete= indexOf(pNodo) == -1 ? false : true ; 
        if(indexOf(pNodo)!=-1) remove(indexOf(pNodo));        
        return delete;
    }
    
    /**
     * Devuelve el valor del nodo localizado en la posición indicada del <tt>LinkedList</tt>
     * @param pIndex indica la posicion que queremos buscar
     * @return el valor del nodo localizado en la posicion indicada
     * @throws IndexOutOfBoundsException si {@code pIndex} está fuera de rango
     */
    public T get(int pIndex){
        if(pIndex<0 || pIndex >=  this.len) throw new IndexOutOfBoundsException("Posicion no valida");
        ListNode nodo= this.head;
        for (int i = 0; i < pIndex; i++) nodo = nodo.next;
        return  (T) nodo.getData();
    }
    
    /**
     * Devuelve la ​posición​ en la que se encuentra el nodo indicado
     * o -1 en caso de no encontrarlo
     * @param pNodo indica el nodo a buscar
     * @return <tt>la posicion del nodo</tt> o <tt>-1</tt> en caso 
     * de no encontrarlo
     */
    public int indexOf(Object pNodo){
        int coincidencia = -1,cont = 0;
        ListNode<T> nodo = head;
        while (nodo != null) {
            if (nodo.equals(pNodo))  coincidencia=cont;                 
            cont++;
            nodo = nodo.next;
        }
         return coincidencia;
    }
    
    /**
     * Añade un elemento al <tt>LinkedList</tt> en la posicion indicada
     * @param pIndex indice de la posicion en la que añadir el nodo
     * @param pNodo nodo a añadir 
     * @throws IndexOutOfBoundsException si {@code pIndex} está fuera de rango
     */
    public void insert(T pNodo, int pIndex){
        if(pIndex<0 || pIndex > this.len) throw new IndexOutOfBoundsException("Posicion no valida");
        if (pIndex == 0) addHead(pNodo);
       else if (pIndex==this.len)  addTail(pNodo);
       else { 
           ListNode<T> nodo = head;
           for (int i = 1; i < pIndex; i++) 
               nodo = nodo.next; 

           ListNode<T> copia = nodo.next; 
           nodo.next = new ListNode<>(pNodo); 
           (nodo.next).next = copia;
           len++; 
        }
    }
    
    /**
     * Vacía el <tt>LinkedList</tt> y vuelve a 0 el numero de elemtos
     */
    public void clear(){
        len=0;
    }
    
    /**
     * Indica si <tt>LinkedList</tt> está vacío o no
     * @return <tt>true</tt> si está vacío y false si contiene algun nodo
     */
    @Override
    public boolean isEmpty(){
        return size() < 1 ? true : false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator(){
            private int posNextElem=0;
            
            @Override
            public boolean hasNext(){
                return (posNextElem<len);
            }
            @Override
            public T next(){
                return (T) get(posNextElem++);
            }
        };
    }
    
    /**
     * Devuelve el nodo situado en la posición indicada y lo elimina
     * del <tt>LinkedList</tt> 
     * @param pIndex indice del nodo a eliminar
     * @return el nodo que se ha eliminado
     * @throws IndexOutOfBoundsException si {@code pIndex} está fuera de rango
     */
    public T remove(int pIndex){
        if(pIndex<0||pIndex>=this.len) throw new IndexOutOfBoundsException("Posicion no valida");
        ListNode<T> headNodo = this.head;
        
        if(pIndex==0){
                headNodo = this.head; 
                this.head = this.head.next; 
                if (this.head == null) this.tail = null; 
        }        
        
        for (int i = 1; i < pIndex; i++) 
                headNodo = headNodo.next;
        
        ListNode<T> newHead = headNodo.next;
        headNodo.next = newHead.next;
        len--;
        return pIndex > 0  ? newHead.data : headNodo.data ;
    }
    
    /**
     * Devuelve el número de nodos contenidos en el <tt>LinkedList</tt>
     * @return el numero de nodos que contiene el <tt>LinkedList</tt>
     */
    public int size(){
        return this.len;
    }
    
    /**
     * Devuelve el nodo localizado en la posición indicada del <tt>LinkedList</tt>
     * @param pIndex indica la posicion que queremos buscar
     * @return el nodo localizado en la posicion indicada
     * @throws IndexOutOfBoundsException si {@code pIndex} está fuera de rango
     */
    private ListNode getNodo(int pIndex){
        if(pIndex<0 || pIndex >=  this.len) throw new IndexOutOfBoundsException("Posicion no valida");
        ListNode nodo= this.head;
        for (int i = 0; i < pIndex; i++) nodo = nodo.next;
        return nodo;
    }
    
    /**
     * Reemplaza el valor del nodo en la posicion indicada con el valor especificado
     * @param pIndex indice del nodo a reemplazar
     * @param pNodo nodo que se almacena en la posicion indicada
     * @throws IndexOutOfBoundsException si {@code pIndex} está fuera de rango
     */
    public void set(int pIndex, T pNodo){
        if(pIndex<0||pIndex>=this.len) throw new IndexOutOfBoundsException("Posicion no valida" );       
        getNodo(pIndex).setData(pNodo);
    }
    
    /**
     * Devuelve un una representacion del objeto en forma de String.
     * el método {@code toString} devuelve generalmente un string 
     * que "textualmente representa" este objeto. el resultado debe 
     * ser conciso pero una representacon informativa facil para la persona
     * que lo lee
     * @return  un String que representa el objeto
     */
    @Override
    public String toString() {
        if(isEmpty()) return "No hay elementos.";
        String toStr= "[ ";
        for (int i = 0; i < size(); i++) 
            toStr +=(i==0) ? "" + get(i) : ", " + get(i); 

        return toStr + " ]";
    }
    
    // stack
    @Override
    public void push(T obj) {
        add(size(),obj);
    }

    @Override
    public Object pop() {        
        return remove(size()-1);
    }

    @Override
    public Object peek() {
        return get(size()-1);
    }
    // queue
    @Override
    public void enqueue(T obj) {
        add(0,obj);
    }

    @Override
    public Object dequeue() {
            return remove(size()-1);
    }
    
    /**
     * Clase <tt>ListNode</tt> que permite crear los objetos a almacenar
     * en <tt>LinkedList</tt> y contiene los nodos a los que apunta para
     * enlazarlos entre si 
     * @param <T> el tipo de elemento devuelto por  <tt>ListNode</tt>
     */
    private class ListNode<T> {
        private T data;
        private ListNode<T> next;
        private ListNode<T> prev;
        
         public ListNode(T pData) {
            this.data = pData;
        }
        
        public T getData(){
            return this.data;
        }
        public void setData(T pData){
            this.data= pData;
        }
        public ListNode<T> getNext(){
            return this.next;
        }
        public ListNode<T> getPrev(){
            return this.prev;
        }
        public void setNext(T pListNext){
            this.next=(ListNode<T>) pListNext;
        }
        public void setPrev(T pListPrev){
            this.prev= (ListNode<T>)  pListPrev;
        }

        @Override
        public boolean equals(Object o){
             return data.equals(o);
        }
        @Override
        public String toString() {
            return ""+ this.data;
        }        
    }
}

